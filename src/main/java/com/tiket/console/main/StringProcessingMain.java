/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tiket.console.main;

import com.tiket.basic.string.processing.StringProcessing;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rizky Perdana
 */
public class StringProcessingMain {
    public static void main(String[] args) {
        StringProcessing processing = new StringProcessing();
        System.out.println(processing.lowerUpperString("Saya sedang Belajar Bahasa PemOgraman JAVA"));
        System.out.println(processing.trim("Saya sedang Belajar Bahasa PemOgraman JAVA"));
        System.out.println(processing.modifyString("Developer PT. Global Tiket Network"));
        try {
            processing.lambdaRead();
        } catch (IOException ex) {
            Logger.getLogger(StringProcessingMain.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
