/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tiket.console.main;

import com.tiket.basic.mhs.contoller.MahasiswaContoller;
import com.tiket.basic.mhs.contoller.ReportController;
/**
 *
 * @author Rizky Perdana
 */
public class MahasiswaMain {
    public static void main(String[] args) {
        MahasiswaContoller.getInstance().inputMahasiswa();
        ReportController.getInstance().generateGradeAndResume(MahasiswaContoller.getInstance().getMahasiswa());
    }
}
