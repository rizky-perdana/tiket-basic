/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tiket.basic.mhs.entity;

/**
 *
 * @author Rizky Perdana
 */
public class Mahasiswa {

    private String nim;
    private String nama;
    private Integer kehadiran;
    private Integer midTest;
    private Integer uas;
    private Integer nilaiAkhir;
    private Character grade;

    public static final int GRADE_MAX_E = 45;
    public static final int GRADE_MAX_D = 60;
    public static final int GRADE_MAX_C = 75;
    public static final int GRADE_MAX_B = 84;
    public static final int GRADE_MAX_A = 100;

    public Mahasiswa() {
    }

    public Mahasiswa(String nim, String nama, Integer kehadiran, Integer midTest, Integer uas) {
        this.nim = nim;
        this.nama = nama;
        this.kehadiran = kehadiran;
        this.midTest = midTest;
        this.uas = uas;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Integer getKehadiran() {
        return kehadiran;
    }

    public void setKehadiran(Integer kehadiran) {
        this.kehadiran = kehadiran;
    }

    public Integer getMidTest() {
        return midTest;
    }

    public void setMidTest(Integer midTest) {
        this.midTest = midTest;
    }

    public Integer getUas() {
        return uas;
    }

    public void setUas(Integer uas) {
        this.uas = uas;
    }

    public Integer getNilaiAkhir() {
        return nilaiAkhir;
    }

    public void setNilaiAkhir(Integer nilaiAkhir) {
        this.nilaiAkhir = nilaiAkhir;
    }

    public Character getGrade() {
        if (null != nilaiAkhir && null == grade) {
            if (nilaiAkhir <= GRADE_MAX_E) {
                grade = 'E';
            } else if (nilaiAkhir <= GRADE_MAX_D) {
                grade = 'D';
            } else if (nilaiAkhir <= GRADE_MAX_C) {
                grade = 'C';
            } else if (nilaiAkhir <= GRADE_MAX_B) {
                grade = 'B';
            } else {
                grade = 'A';
            }
        }
        return grade;
    }

    public void setGrade(Character grade) {
        this.grade = grade;
    }
}
