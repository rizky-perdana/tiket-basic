/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tiket.basic.mhs.entity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Rizky Perdana
 */
public class Report {
    private List<Mahasiswa> mahasiswas;
    private int lulus;
    private int tidakLulus;

    public List<Mahasiswa> getMahasiswas() {
        if (null==mahasiswas) {
            mahasiswas = new ArrayList<>();
        }
        return mahasiswas;
    }

    public void setMahasiswas(List<Mahasiswa> mahasiswas) {
        this.mahasiswas = mahasiswas;
    }

    public int getLulus() {
        return lulus;
    }

    public void setLulus(int lulus) {
        this.lulus = lulus;
    }

    public int getTidakLulus() {
        return tidakLulus;
    }

    public void setTidakLulus(int tidakLulus) {
        this.tidakLulus = tidakLulus;
    }
    
}
