/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tiket.basic.mhs.contoller;

import com.tiket.basic.mhs.entity.Mahasiswa;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Rizky Perdana
 */
public class MahasiswaContoller {

    private static MahasiswaContoller instance;
    private static List<Mahasiswa> mahasiswa;

    private MahasiswaContoller() {
    }

    public static synchronized MahasiswaContoller getInstance() {
        if (instance == null) {
            instance = new MahasiswaContoller();
            mahasiswa = new ArrayList<>();
        }
        return instance;
    }

    public List<Mahasiswa> getMahasiswa() {
        return mahasiswa;
    }
    
    public List<Mahasiswa> inputMahasiswa(){
        String nim;
        String nama;
        int kehadiran;
        int mid;
        int uas;
        Scanner sc = new Scanner(System.in);
        System.out.print("Masukkan jumlah mahasiswanya: ");
        int input = sc.nextInt();
        for (int i = 0; i < input; i++) {
            System.out.print("Masukkan NIM: ");
            nim = sc.next();
            System.out.print("Masukkan Nama: ");
            nama = sc.next();
            System.out.print("Masukkan Nilai Kehadiran: ");
            kehadiran = sc.nextInt();
            System.out.print("Masukkan Nilai Mid Test: ");
            mid = sc.nextInt();
            System.out.print("Masukkan Nilai UAS: ");
            uas = sc.nextInt();
            MahasiswaContoller.mahasiswa.add(new Mahasiswa(nim, nama, kehadiran, mid, uas));
        }
        return MahasiswaContoller.mahasiswa;
    }

}
