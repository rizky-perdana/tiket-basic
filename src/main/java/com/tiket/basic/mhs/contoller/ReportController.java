/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tiket.basic.mhs.contoller;

import com.tiket.basic.mhs.entity.Mahasiswa;
import com.tiket.basic.mhs.entity.Report;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Rizky Perdana
 */
public class ReportController {

    private static ReportController instance;
    private final String FILE_LOCATION = "dist/report/";
    private final String FILENAME = "mahasiswa.nilai";

    private ReportController() {
    }

    public static synchronized ReportController getInstance() {
        if (null == instance) {
            instance = new ReportController();
        }
        return instance;
    }

    public void generateGradeAndResume(List<Mahasiswa> mahasiswa) {
        Report report = new Report();
        mahasiswa.stream().filter((mhs) -> (null != mhs.getKehadiran()
                && null != mhs.getMidTest()
                && null != mhs.getUas())).map((mhs) -> {
                    Double result = (0.2 * mhs.getKehadiran())
                            + (0.4 * mhs.getMidTest())
                            + (0.4 * mhs.getUas());
                    mhs.setNilaiAkhir(result.intValue());
            return mhs;
        }).map((mhs) -> {
            if (mhs.getNilaiAkhir() <= Mahasiswa.GRADE_MAX_D) {
                report.setTidakLulus(report.getTidakLulus() + 1);
            } else {
                report.setLulus(report.getLulus() + 1);
            }
            return mhs;
        }).forEachOrdered((mhs) -> {
            report.getMahasiswas().add(mhs);
        });
        StringBuffer sb = getResume(report);
        System.out.println(sb);
        saveResume(sb);
    }

    private StringBuffer getResume(Report report) {
        StringBuffer sb = new StringBuffer();
        sb.append("No.\tNIM\t\tNama\t\tNilai Akhir\tGrade\n");
        sb.append("===========================================================\n");
        for (int i = 0; i < report.getMahasiswas().size(); i++) {
            Mahasiswa mhs = report.getMahasiswas().get(i);
            sb.append((i + 1)).append("\t").append(mhs.getNim()).append("\t").append(mhs.getNama()).append("\t\t").append(mhs.getNilaiAkhir()).append("\t\t").append(mhs.getGrade()).append("\n");
        }
        sb.append("===========================================================\n");
        sb.append("Jumlah Mahasiswa : ").append(report.getMahasiswas().size()).append(" (berdasarkan hasil kalkulasi)\n");
        sb.append("Jumlah Mahasiswa yg Lulus : ").append(report.getLulus()).append(" (berdasarkan hasil kalkulasi)\n");
        sb.append("Jumlah Mahasiswa yg Tidak Lulus : ").append(report.getTidakLulus()).append(" (berdasarkan hasil kalkulasi)\n");
        sb.append("===========================================================");
        return sb;
    }

    private void saveResume(StringBuffer sb) {
        FileWriter fileWriter = null;
        try {
            File output = new File(FILE_LOCATION);
            if (!output.isDirectory()) {
                output.mkdir();
            }
            output = new File(FILE_LOCATION + FILENAME);
            if (output.exists()) {
                output.delete();
            }
            output.createNewFile();
            fileWriter = new FileWriter(output);
            fileWriter.write(sb.toString());
        } catch (IOException ex) {
            Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (fileWriter != null) {
                    fileWriter.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(ReportController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
