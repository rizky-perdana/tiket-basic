/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tiket.basic.string.processing;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.AbstractMap.SimpleEntry;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toMap;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Rizky Perdana
 */
public class StringProcessing {

    public String lowerUpperString(String input) {
        String output = "";
        for (int i = 0; i < input.length(); i++) {
            if (Character.isLowerCase(input.charAt(i))) {
                output = output + Character.toString(input.charAt(i)).toUpperCase();
            } else {
                output = output + Character.toString(input.charAt(i)).toLowerCase();
            }
        }
        return output;
    }

    public String trim(String input) {
        char temp;
        for (int i = 0; i < input.length(); i++) {
            temp = Character.toLowerCase(input.charAt(i));
            if ('a' == temp || 'i' == temp || 'u' == temp || 'e' == temp || 'o' == temp) {
                input = input.replaceAll("[" + Character.toString(temp).toUpperCase() + Character.toString(temp).toLowerCase() + "]", "");
            }
        }
        return input;
    }

    public String modifyString(String input) {
        input = input.replaceAll(" ", "");
        String[] removedAplh = new String[]{"e", "l", "o", "r", "T", "k", "t"};
        int[] alphabetCount = new int[removedAplh.length];
        //count and delete 
        for (int i = 0; i < alphabetCount.length; i++) {
            alphabetCount[i] = StringUtils.countMatches(input, removedAplh[i]);
        }
        Pattern p;
        Matcher m;
        for (int i = 0; i < alphabetCount.length; i++) {
            if (!"r".equals(removedAplh[i])) {
                p = Pattern.compile("\\G((?!^).*?|[^" + removedAplh[i] + "]*" + removedAplh[i] + ".*?)" + removedAplh[i] + "");
                m = p.matcher(input);
                input = m.replaceAll("$1");
            }
        }

//        input = input.replaceAll("[eloTkt]", "");
//        System.out.println("RESULT: "+input);
        StringBuilder output = new StringBuilder(input);

        for (int i = 0; i < alphabetCount.length; i++) {
            output.insert((output.indexOf(removedAplh[i]) + 1), alphabetCount[i]);
        }
        return output.toString();
    }

    private static final String TXT_RESOURCES_PATH = "src/resources/article.txt";

    public void lambdaRead() throws IOException {
        Path path = Paths.get(TXT_RESOURCES_PATH);
        Map<String, Integer> wordCount = Files.lines(path).flatMap(line -> Arrays.stream(line.trim().split(" ")))
                .map(word -> word.replaceAll("[^a-zA-Z]", "").toLowerCase().trim())
                .filter(word -> word.length() > 0)
                .map(word -> new SimpleEntry<>(word, 1))
                .collect(toMap(e -> e.getKey(), e -> e.getValue(), (v1, v2) -> v1 + v2));
        System.out.println("Total words from text: " + wordCount.size());
        System.out.println("=================================================");
        System.out.println("Total Appearence of Each Word: ");
        wordCount.forEach((k, v) -> System.out.println(k +": "+ v));
        System.out.println("=================================================");
        
        Map<String, Integer> occurOnce = wordCount.entrySet().stream()
                .filter(x -> x.getValue() == 1)
                .collect(toMap(x -> x.getKey(), x -> x.getValue()));
        System.out.println("Total words that only appear one time: " + occurOnce.size());
        System.out.println("=================================================");
        Map<String, Integer> frequentlyUsed = wordCount.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).limit(1)
                .collect(toMap(x -> x.getKey(), x -> x.getValue()));
        System.out.println("Frequently used word: "+frequentlyUsed.size() +" ->");
        frequentlyUsed.forEach((k, v) -> System.out.println(k +": "+ v));
        System.out.println("=================================================");
        System.out.println("The least used word: ");
        wordCount.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.naturalOrder())).limit(occurOnce.size())
				.forEach(e -> System.out.println(e.getKey() + ": " + e.getValue()));

    }
}
